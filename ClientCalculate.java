// Java program to illustrate Client Side Programming 
// for Simple Calculator using TCP 
package clientcalculate;


import java.io.*; 
import java.net.*;
import java.util.Scanner; 

public class ClientCalculate 
{ 
	public static void main(String[] args) throws IOException 
	{ 
		InetAddress ip = InetAddress.getLocalHost(); 
		int port = 4444; 
		Scanner sc = new Scanner(System.in); 

		 
		Socket s = new Socket(ip, port); 
		DataInputStream dis = new DataInputStream(s.getInputStream()); 
		DataOutputStream dos = new DataOutputStream(s.getOutputStream()); 

		while (true) 
		{ 
			System.out.print("Masukkan operand dan operator dengan format : "); 
			System.out.println("'operand operator operand'"); 

			String inp = sc.nextLine(); 

			if (inp.equals("bye")) 
				break; 
                        
			dos.writeUTF(inp); 

                        String ans = dis.readUTF(); 
			System.out.println("Jawaban =" + ans); 
		} 
	} 
} 
