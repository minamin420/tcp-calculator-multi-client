// Java program to illustrate Server Side Programming s
// for Simple Calculator using TCP 
package servercalclulate;


import java.io.*;
import java.net.*;
import java.util.StringTokenizer; 

public class ServerCalclulate 
{ 
	
	public static void main(String args[]) throws IOException 
	{ 
    
		ServerSocket ss = new ServerSocket(4444);
                
			Socket client_1 = ss.accept();

			if (client_1.isConnected()) {

				System.out.println("\nClient 1 (" + (client_1.getLocalAddress().toString()).substring(1) + ":"

						+ client_1.getLocalPort() + ") has joined");

			}
			Socket client_2 = ss.accept();
			if (client_2.isConnected()) {

				System.out.println("Client 2 (" + (client_2.getLocalAddress().toString()).substring(1) + ":"

						+ client_2.getLocalPort() + ") has joined");

			}
		 
		DataInputStream dis = new DataInputStream(client_1.getInputStream()); 
		DataOutputStream dos = new DataOutputStream(client_1.getOutputStream()); 
                
                DataInputStream dis2 = new DataInputStream(client_2.getInputStream()); 
		DataOutputStream dos2 = new DataOutputStream(client_2.getOutputStream());
                
		while (true) 
		{ 
			 
			String input = dis.readUTF(); 
			String input2 = dis2.readUTF(); 

			if(input.equals("bye") || input2.equals("bye")) 
				break; 

			System.out.println("Input Client 1: " + input);
                        System.out.println("Input Client 2: " + input2);
			int resClient_1;
                        int resClient_2; 
 
			StringTokenizer st = new StringTokenizer(input); 
                        StringTokenizer st2 = new StringTokenizer(input2); 
                        
			int oprnd1 = Integer.parseInt(st.nextToken()); 
			String operation = st.nextToken(); 
			int oprnd2 = Integer.parseInt(st.nextToken());
                        
                        int oprnd3 = Integer.parseInt(st2.nextToken()); 
			String operation2 = st2.nextToken(); 
			int oprnd4 = Integer.parseInt(st2.nextToken());

			if (operation.equals("+") || operation2.equals("+")) 
			{ 
                            resClient_1 = oprnd1 + oprnd2; 
                            resClient_2 = oprnd3 + oprnd4;
			} 

			else if (operation.equals("-") || operation2.equals("-")) 
			{ 
                            resClient_1 = oprnd1 - oprnd2; 
                            resClient_2 = oprnd3 - oprnd4; 
			} 
			else if (operation.equals("*") || operation2.equals("*")) 
			{ 
                            resClient_1 = oprnd1 * oprnd2; 
                            resClient_2 = oprnd3 * oprnd4;
			} 
			else
			{ 
                            resClient_1 = oprnd1 / oprnd2; 
                            resClient_2 = oprnd3 / oprnd4;
			} 
			System.out.println("Mengirim Hasil"); 

			dos.writeUTF(Integer.toString(resClient_1)); 
			dos2.writeUTF(Integer.toString(resClient_2)); 

                } 
	} 
} 
